**************************************************
**************************************************
# Useful Tips:
 - When creating a project, create the root folder and use '$ git init' to initialize it.
 - Populate the project with the initial directory setup and files.
 - Check all the changes that were made using '$ git status'.
 - Use '$ git add <change_name>' to add the files to commit.
 - Commit the changes and add a message such as "Initial Commit" using '$ git commit -m "<commit_message>"'.

## Another method for creating the initial local repo is to use Git-Flow
 - Install gitflow:
<https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow>
 - Then follow the commands there to initialize the local repo.
 - Push the local repo to remote.

## If using with Gitlab
 - Create a project on Gitlab.
 - Make the initial commit and push to the remote using '$ git push <URL>' (This may need to be tweaked, I haven't built that many projects using Gitlab yet.)
 - You should be able to push and pull from the remote now.

 - Once the project is underway it is recommended to use the issues tab inside Gitlab to create new branches to pull and push from.
 - After the issues have been created and a new 'merge request + branch' has been created, use '$ git fetch' to view the remote changes locally then use '$ git checkout -b <name_of_remote_branch_to_work_on> origin/<name_of_remote_branch_to_work_on>' to start developing in that branch
 - Once the necessary changes have been made, committed, and pushed to the branch the issue can be taken out of "WIP status" and reviewed before merging to the remote develop branch.


**************************************************
## To initialize a project folder with git.
 - Make new files and prepare the project folder.

```
git init
```


**************************************************
## Tracking files that were created/modified so they are ready to commit.
```
git add <new_files>
```


**************************************************
## Committing changes that were tracked. Add messages with the "-m" flag.
```
git commit -m "<commit_message>"
```
## 'Squishing' new changes to the previous commit.
```
git commit --amend
```

 - Adjust the commit message as necessary then save with CTRL+O then quit with CTRL+X.

**************************************************
## Updating the remote repo branch with the committed changes with default settings.
```
git push <address_of_project>
```
## Updating a specific remote branch.
```
git push origin <target_branch>
```


**************************************************
## Pushing a new local branch to remote so remote tracks it.
```
git push -u origin <new_branch>
```


**************************************************
## Update your current branch with default branches.
```
git pull
```
## Update your current branch with a branch on the remote repo.
```
git pull origin <branch_name>
```


**************************************************
## Checking the current status of git to see the tracked files ready to commit and that have been committed.
```
git status
```


**************************************************
## Checking the list of local branches.
```
git branch
```
## Creating a new branch locally.
```
git branch <branch_name>
```
## Removing a branch.
```
git branch -d <branch_name>
```


**************************************************
## Changing branches.
```
git checkout <branch_name>
```
## Changing branches and make it if it doens't exist locally.
```
git checkout -b <branch_name>
```


**************************************************
## Update the project to connect it to a remote repo.
```
git remote add origin <remote_URL>
```
## Checking the remote repo and project tracking state.
```
git remote show origin
```


**************************************************
## Check for remote updates.
```
git fetch
```

**************************************************
## Merging a branch to your current branch.
```
git merge <branch_name>
```
## Merging a remote branch to your current local branch.
```
git merge <branch_name> origin/<source_branch>
```


**************************************************
**************************************************
## Default Gitlab Commands.
## Command line instructions
# Git global setup

```
git config --global user.name "Johann Diep"
git config --global user.email "johann@psygig.com"
```

# Create a new repository

```
git clone git@gitlab.com:psygig/labs/experiments/gitcommands.git
cd gitcommands
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

# Existing folder

```
cd existing_folder
git init
git remote add origin git@gitlab.com:psygig/labs/experiments/gitcommands.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

# Existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:psygig/labs/experiments/gitcommands.git
git push -u origin --all
git push -u origin --tags
```

